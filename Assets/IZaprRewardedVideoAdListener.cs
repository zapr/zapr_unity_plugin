/*******************************************************
 * Copyright (C) 2016-2017 Red Brick Lane Marketing Pvt Ltd - All Rights Reserved
 * 
 * This file is part of Zapr Unity Plugin and is being maintained by Udayan.
 * 
 * Zapr Unity Plugin can not be copied and/or distributed without the explicit
 * permission of  Red Brick Lane Marketing Pvt Ltd
 *******************************************************/

using UnityEngine;
using System.Collections;

/** Zapr Video ad Events listener */
public interface IZaprRewardedVideoAdListener {

    // Called when ad response is received from server. 
    // If pre-caching is disabled, then you can call cacheVideoAd() method after receiving this callback.
    void onRewardVideoResponseReceived(string vastXML);
    
    // Called when Video ad is ready to be displayed to user. 
	void onRewardVideoReady(string vastXML);
	
	// Called when video ad has started playing
	// Game developer should pause the game at this point if not paused already
	void onRewardVideoStarted();

	// Called when video ad is clicked by the user
	void onRewardVideoClicked();

	// Called when video ad is finished playing.
	// Game developer should assign game rewards to user after this callback.
	void onRewardVideoFinished();

	// Called when video ad player is closed. 
	// Game developers should resume (un-pause) game play at this point.
	void onRewardVideoPlayerClosed();

	// Called if there is any error while requesting or playing video ad
	void onRewardVideoError(string error);

	// Called if video ad gratified
	void onRewardVideoGratified(string rewardType, double rewardAmount);
}
