/*******************************************************
 * Copyright (C) 2016-2017 Red Brick Lane Marketing Pvt Ltd - All Rights Reserved
 * 
 * This file is part of Zapr Unity Plugin and is being maintained by Udayan.
 * 
 * Zapr Unity Plugin can not be copied and/or distributed without the explicit
 * permission of  Red Brick Lane Marketing Pvt Ltd
 *******************************************************/

using UnityEngine;
using System.Collections;

/** Zapr Interstitial ad Events listener */
public interface IZaprInterstitialAdListener {

	// Called when Interstitial ad is ready to be rendered
	void onInterstitialAdReady();

	// Called when Interstitial ad is shown to the user
	void onInterstitialAdShown();

	// Called when Interstitial ad is clicked by the user
	void onInterstitialAdClicked();

	// Called when Interstitial ad is closed
	void onInterstitialAdClosed();

	// Called if there is any error while requesting Interstitial ad
	void onInterstitialAdError(string error);
}
