﻿/*******************************************************
 * Copyright (C) 2016-2017 Red Brick Lane Marketing Pvt Ltd - All Rights Reserved
 * 
 * This file is part of Zapr Unity Plugin and is being maintained by Udayan.
 * 
 * Zapr Unity Plugin can not be copied and/or distributed without the explicit
 * permission of  Red Brick Lane Marketing Pvt Ltd
 *******************************************************/

using UnityEngine;
using System.Collections;

// Zapr Data SDK Plugin
public class ZaprDataSdkPlugin : MonoBehaviour
{
    private AndroidJavaObject zaprDataSdkPlugin = null;
    private AndroidJavaObject appContext;


    void Start()
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        appContext = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        zaprDataSdkPlugin = new AndroidJavaObject("com.redbricklane.zapr.unity.datasdk.ZaprDataSdkPlugin", appContext);
    }

    // Starts the Zapr Service
    public void startZaprService()
    {

#if UNITY_ANDROID || DEBUG
        appContext = GetAppContext();
        // Start Zapr service
        zaprDataSdkPlugin.Call("start");
        Debug.Log("Start Zapr service");
#endif
    }

    private AndroidJavaObject GetAppContext()
    {
        if (appContext == null)
        {
            appContext = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            zaprDataSdkPlugin = new AndroidJavaObject("com.redbricklane.zapr.unity.datasdk.ZaprDataSdkPlugin", appContext);
            Debug.Log("Fetched appcontext");

        }
        else
        {
            Debug.Log("Error in fetching appcontext");
        }
        return appContext;
    }

    public void enableDebug(bool isDebugEnabled)
    {

#if UNITY_ANDROID || DEBUG
        appContext = GetAppContext();
        if (appContext != null)
        {
            zaprDataSdkPlugin.Call("enableDebug", isDebugEnabled);
        }
#endif
    }

    // kill  Zapr Service
    public void killZaprService()
    {
#if UNITY_ANDROID || DEBUG

        appContext = GetAppContext();
        if (appContext != null)
        {
            // Kill Zapr Service
            zaprDataSdkPlugin.Call("killSDK");
        }
#endif
    }

    // OptOut of Zapr Service
    public void optOutOfZaprService()
    {
#if UNITY_ANDROID || DEBUG
        appContext = GetAppContext();
        if (appContext != null)
        {
            // Opt out of Zapr Service
            zaprDataSdkPlugin.Call("optOut");
        }
#endif
    }

    // Set location access enabled
    public void setLocationAccess(bool isEnabled)
    {
#if UNITY_ANDROID || DEBUG
        appContext = GetAppContext();
        if (appContext != null)
        {
            // Opt out of Zapr Service
            zaprDataSdkPlugin.Call("setLocationAccess", isEnabled);
        }
#endif
    }

    // OptIn back to Zapr Service (only if Opted Out earlier)
    // Call this to opt in back to Zapr service only if opted out earlier.
    public void optInToZaprService()
    {
#if UNITY_ANDROID || DEBUG
        appContext = GetAppContext();
        if (appContext != null)
        {
            // Opt In back to Zapr Service
            zaprDataSdkPlugin.Call("optIn");
        }
#endif
    }
}
