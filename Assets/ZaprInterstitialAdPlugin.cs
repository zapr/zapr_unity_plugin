/*******************************************************
 * Copyright (C) 2016-2017 Red Brick Lane Marketing Pvt Ltd - All Rights Reserved
 * 
 * This file is part of Zapr Unity Plugin and is being maintained by Udayan.
 * 
 * Zapr Unity Plugin can not be copied and/or distributed without the explicit
 * permission of  Red Brick Lane Marketing Pvt Ltd
 *******************************************************/

using UnityEngine;
using System.Collections;

// Zapr Interstitial Ad Plugin
public class ZaprInterstitialAdPlugin : MonoBehaviour
{
	private AndroidJavaObject activityContext = null;
	private AndroidJavaObject zaprInterstitialPlugin = null;
	private bool isTestMode = false;
	private IZaprInterstitialAdListener listener;
	private IZaprAdStatusListener mAdStatusListener;
	private GameObject gameObj;
	private string gameObjName;
	private string customAdServerUrl = null;
	// Default
	private string backgroundColor = "#DD000000";
	private bool permissionRequestEnabled = false;
	// User Age in years
	private int userAge = -1;
	// User gender. Options: m / f
	private string userGender = null;

	public ZaprInterstitialAdPlugin ()
	{
		#if UNITY_ANDROID || DEBUG
		activityContext = new AndroidJavaClass ("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject> ("currentActivity");
		zaprInterstitialPlugin = new AndroidJavaObject ("com.redbricklane.zapr.unity.banner.ZaprInterstitialPlugin");
		#endif
	}

	// Call this method to fetch Interstitial ad from server.
	// It is mandatory to call this method before calling showInterstitialAd() method.
	public void loadInterstitialAd (string adUnitId, IZaprInterstitialAdListener interstitialListener)
	{
		#if UNITY_ANDROID || DEBUG
		listener = interstitialListener;
		if (adUnitId != null) {
			string handlerName = gameObject.name;


			// Setting optional parameters before calling loadAd
			if (isTestMode) {
				zaprInterstitialPlugin.Call ("setTestModeEnabled", isTestMode);
			}
			if (customAdServerUrl != null) {
				zaprInterstitialPlugin.Call ("setAdServerUrl", customAdServerUrl);
			}
			if (permissionRequestEnabled) {
				zaprInterstitialPlugin.Call ("setPermissionRequestEnabled", permissionRequestEnabled);
			}
			if (userAge > 0 || userGender != null) {
				zaprInterstitialPlugin.Call ("setUserInfo", new object[2]{ userAge, userGender });
			}
			zaprInterstitialPlugin.Call ("setBackgroundColor", backgroundColor);

			// Call for ad
			zaprInterstitialPlugin.Call ("loadInterstitialAd", new object[3]{ activityContext, adUnitId, handlerName });
		} else {
			Debug.Log ("AdUnit id is invalid");
		}
		#endif
	}

	// Call this method to register for ad status
	public void setAdStatusListener(IZaprAdStatusListener adStatusListener)
	{
		#if UNITY_ANDROID
		mAdStatusListener = adStatusListener;
		#endif
	}

	// Call this method to show Interstitial ad to user.
	// Call this method only after receiving onInterstitialAdReady() callback
	public void showInterstitialAd ()
	{
		#if UNITY_ANDROID
		zaprInterstitialPlugin.Call ("showInterstitialAd");
		#endif
	}

	// Call this method before closing the application.
	// This cleans up resources used by Zapr Interstitial Ad SDK
	public void destroy ()
	{
		#if UNITY_ANDROID
		if (zaprInterstitialPlugin != null) {
			zaprInterstitialPlugin.Call ("destroyInterstitialAd");
		}
		#endif
	}

	// (optional) Set the log level of Zapr SDK
	// Default log level is : error
	public void setLogLevel (string logLevel)
	{
		if (logLevel != null) {
			logLevel = logLevel.ToLower ();
			if (logLevel.Equals ("verbose")
			    || logLevel.Equals ("debug")
			    || logLevel.Equals ("info")
			    || logLevel.Equals ("warn")
			    || logLevel.Equals ("error")
			    || logLevel.Equals ("none")) {
				zaprInterstitialPlugin.Call ("setLogLevel", logLevel);
			}
		}
	}

	// (optional) Enable test mode to request test ads during integrtion.
	// Note: Test mode must be disabled in production
	public void setTestModeEnabled (bool enableTestMode)
	{
		isTestMode = enableTestMode;
	}

	// (Optional) Set custom ad server URL if required.
	public void setAdServerUrl (string adServerUrl)
	{
		customAdServerUrl = adServerUrl;
	}

	// (Optional) Set the background color for Interstitial ad.
	// Default background color is black.
	// Format: #AARRGGBB Eg: #AAFFFFFF(semi transparent white color)
	public void setInterstitialAdBackgroundColor (string colorString)
	{
		backgroundColor = colorString;
	}

	// (Optional) Enable Rumtime permission request for Android M+ devices.
	// If enabled, runtime permissions will be requested to user when ad is first called.
	// Disabled by default.
	public void enableRuntimePermissionRequest (bool enable)
	{
		permissionRequestEnabled = enable;
	}

	// (Optional) Set user age and gender in ad request.
	// If user age and gender is passed, more targeted ads can be shown to user.
	// Note: Pass user age / gender only if correct user info is available.
	public void setUserInfo (int age, string gender)
	{
		if (age > 0 && age < 120) {
			userAge = age;
		}
		if (gender != null) {
			userGender = gender;
		}
	}

	#region Android Callbacks

	void onZaprInterstitialAdLoaded (string message)
	{
		if (listener != null) {
			listener.onInterstitialAdReady ();
		}
	}

	void onZaprInterstitialAdShown (string message)
	{
		if (listener != null) {
			listener.onInterstitialAdShown ();
		}
	}

	void onZaprInterstitialAdClicked (string message)
	{
		if (listener != null) {
			listener.onInterstitialAdClicked ();
		}
	}

	void onZaprInterstitialAdClosed (string message)
	{
		if (listener != null) {
			listener.onInterstitialAdClosed ();
		}
	}

	void onZaprFailedToLoadInterstitialAd (string message)
	{
		if (listener != null) {
			listener.onInterstitialAdError (message);
		}
	}

	#endregion

	#region Android Ad Status Callbacks
	void onZaprAdAlreadyLoaded(string adUnitId)
	{
		if(mAdStatusListener != null){
			mAdStatusListener.onAdAlreadyLoaded (adUnitId);
		}
	}
	void onZaprAdRequestBlocked(string response)
	{			
		if(mAdStatusListener != null){
			string[] s = response.Split ('|');
			mAdStatusListener.onAdRequestBlocked (int.Parse(s[0]),s[1]);
		}	
	}
	#endregion
}

