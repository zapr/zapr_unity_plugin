﻿/*******************************************************
 * Copyright (C) 2016-2017 Red Brick Lane Marketing Pvt Ltd - All Rights Reserved
 * 
 * This file is part of Zapr Unity Plugin and is being maintained by Udayan.
 * 
 * Zapr Unity Plugin can not be copied and/or distributed without the explicit
 * permission of  Red Brick Lane Marketing Pvt Ltd
 *******************************************************/

using UnityEngine;
using System.Collections;

public interface IZaprAdStatusListener{

	//Called when new ad request is processed by using loadInterstitialAd / loadVideoAd api, and previously loaded ad
	// is not yet consumed
	void onAdAlreadyLoaded(string adUnitId);

	//Called in case of frequest ad request
	void onAdRequestBlocked(int seconds,string adUnitId);
}
