﻿/*******************************************************
 * Copyright (C) 2016-2017 Red Brick Lane Marketing Pvt Ltd - All Rights Reserved
 * 
 * This file is part of Zapr Unity Plugin and is being maintained by Udayan.
 * 
 * Zapr Unity Plugin can not be copied and/or distributed without the explicit
 * permission of  Red Brick Lane Marketing Pvt Ltd
 *******************************************************/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ZaprSampleAppScript : MonoBehaviour, IZaprInterstitialAdListener, IZaprVideoAdListener, IZaprAdStatusListener, IZaprRewardedVideoAdListener
{

	private static AndroidJavaClass unityPlayer;
	private static AndroidJavaObject activity;

	// Declare Zapr Plugins
	private ZaprInterstitialAdPlugin interstitialPlugin;
	private ZaprVideoAdPlugin videoPlugin;
	private ZaprRewardedVideoAdPlugin rewardedVideoPlugin;
	private ZaprDataSdkPlugin zaprDataPlugin;
    private ZaprHelperScript zaprHelperScript;

	void Start ()
	{
        // Initialize Zapr Plugins in Start() method

        #if UNITY_ANDROID
        interstitialPlugin = gameObject.AddComponent<ZaprInterstitialAdPlugin> () as ZaprInterstitialAdPlugin;
		videoPlugin = gameObject.AddComponent<ZaprVideoAdPlugin> () as ZaprVideoAdPlugin;
		zaprDataPlugin = gameObject.AddComponent<ZaprDataSdkPlugin> () as ZaprDataSdkPlugin;
		rewardedVideoPlugin = gameObject.AddComponent<ZaprRewardedVideoAdPlugin> () as ZaprRewardedVideoAdPlugin;
		zaprHelperScript = gameObject.AddComponent<ZaprHelperScript>() as ZaprHelperScript;
        if (Application.platform == RuntimePlatform.Android)
        {
            zaprDataPlugin.startZaprService();
        }
        #endif
    }

        void Update ()
	{
		if (Input.GetKeyUp (KeyCode.Escape)) {
			// Destroy Zapr Plugins before app exists
			destroyZaprPlugins (); 
			Application.Quit ();
		}
	}

	public void setLocationAccess(){
		zaprDataPlugin.setLocationAccess(true);
	}

	// Destroy Zapr Plugins before application exists.
	private void destroyZaprPlugins ()
	{
		if (interstitialPlugin != null) {
			interstitialPlugin.destroy ();
		}
		if (videoPlugin != null) {
			videoPlugin.destroy ();
		}
		if (rewardedVideoPlugin != null) {
			rewardedVideoPlugin.destroy ();
		}
	}

	#region Button onClock Events

	public void loadInterstitialAd ()
	{
		interstitialPlugin.enableRuntimePermissionRequest (false);
		// Note: Do not enable logs in production app. To disable logs, remove following method.
		interstitialPlugin.setLogLevel ("verbose"); 
		// Format of color string : #AARRGGBB
		interstitialPlugin.setInterstitialAdBackgroundColor ("#AAFFFFFF");
		interstitialPlugin.setAdStatusListener (this);
		// Note: Use following adUnits only for testing.
		// To test these adUnits, set your Android application bundle/package name as "com.zapr.unitydemo"
		interstitialPlugin.loadInterstitialAd ("a7f2856a-c6ed-4df3-9697-0df99fe2f07e", this);
	}

	public void showInterstitialAd ()
	{
		interstitialPlugin.showInterstitialAd ();
	}

	public void loadVideoAd ()
	{
		videoPlugin.enableRuntimePermissionRequest (false);
		// Note: Do not enable logs in production app. To disable logs, remove following method.
		videoPlugin.setLogLevel ("verbose");
		videoPlugin.setAdStatusListener (this);
		// Note use following adUnits only for testing.
		// To test these adUnits, set your Android application bundle/package name as "com.zapr.unitydemo"
		videoPlugin.loadVideoAd ("1b8c7e92-134d-452b-a095-143a9e7ae7f1", this);
	}

	public void showVideoAd ()
	{
		videoPlugin.showVideoAd ();
	}

	public void loadRewardedVideoAd ()
	{
		rewardedVideoPlugin.enableRuntimePermissionRequest (false);
		// Note: Do not enable logs in production app. To disable logs, remove following method.
		rewardedVideoPlugin.setLogLevel ("verbose");
		rewardedVideoPlugin.setAdStatusListener (this);
		// Note use following adUnits only for testing.
		// To test these adUnits, set your Android application bundle/package name as "com.zapr.unitydemo"
		rewardedVideoPlugin.loadVideoAd ("84ca74cf-b47d-4f72-b1a4-0fca4c5fcc8e", this);
	}

	public void showRewardedVideoAd ()
	{
		rewardedVideoPlugin.showVideoAd ();
	}

	public void startZaprDataClicked ()
	{
        zaprDataPlugin.enableDebug(false);
        zaprDataPlugin.startZaprService ();
		showToast ("Starting Zapr Service");
	}

    public void optInZaprDataClicked()
    {
        zaprDataPlugin.optInToZaprService();
        showToast("Opt In Service");
    }

    public void optOutZaprDataClicked()
    {
        zaprDataPlugin.optOutOfZaprService();
        showToast("Opt Out Service");
    }

	public void killZaprDataClicked ()
	{
		zaprDataPlugin.killZaprService ();
		showToast ("Kill Zapr Service");
	}

	#endregion

	#region IZaprInterstitialAdListener implementation

	public void onInterstitialAdReady ()
	{
		Debug.Log ("onInterstitialAdReady");
		showToast ("Interstitial Ad Ready");
	}

	public void onInterstitialAdShown ()
	{
		Debug.Log ("onInterstitialAdShown");
	}

	public void onInterstitialAdClicked ()
	{
		Debug.Log ("onInterstitialAdClicked");
	}

	public void onInterstitialAdClosed ()
	{
		ZaprUiUtils zaprUiUtils = new ZaprUiUtils ();
		zaprUiUtils.updateUi ();
		Debug.Log ("onInterstitialAdClosed");
	}

	public void onInterstitialAdError (string error)
	{
		Debug.Log ("onInterstitialAdError");
	}

    #endregion

    #region IZaprVideoAdListener implementation

    public void onResponseReceived(string vastXML)
    {
        Debug.Log("onResponseReceived");
        showToast("Video Ad response received");
    }

    public void onVideoAdReady (string vastXML)
	{
		Debug.Log ("onVideoAdReady");
		showToast ("Video Ad Ready");
	}

	public void onVideoAdStarted ()
	{
		Debug.Log ("onVideoAdStarted");
	}

	public void onVideoAdClicked ()
	{
		Debug.Log ("onVideoAdClicked");
	}

	public void onVideoAdFinished ()
	{
		Debug.Log ("onVideoAdFinished");
	}

	public void onVideoPlayerClosed ()
	{
        Debug.Log ("onVideoPlayerClosed");
	}

	public void onVideoAdError (string error)
	{
		Debug.Log ("onVideoAdError Error: " + error);
	}

	#endregion

	#region IZaprRewardedVideoAdListener implementation

	public void onRewardVideoResponseReceived(string vastXML)
	{
		Debug.Log ("onRewardVideoResponseReceived ");
		showToast ("onRewardVideoResponseReceived");
	}
	public void onRewardVideoReady(string vastXML)
	{
		Debug.Log ("onRewardVideoReady ");
		showToast ("onRewardVideoReady");
	}
	public void onRewardVideoStarted()
	{
		Debug.Log ("onRewardVideoStarted ");
	}
	public void onRewardVideoClicked()
	{
		Debug.Log ("onRewardVideoClicked ");
	}
	public void onRewardVideoFinished(){
		Debug.Log ("onRewardVideoFinished ");
	}
	public void onRewardVideoPlayerClosed()
	{
		Debug.Log ("onRewardVideoPlayerClosed ");
		showToast ("onRewardVideoPlayerClosed");
	}
	public  void onRewardVideoError(string error){
		Debug.Log ("onRewardVideoError ");
	}
	public void onRewardVideoGratified(string rewardType, double rewardAmount){
		showToast ("Reward: "+rewardType+", Reward Amt: "+rewardAmount);
	}

	#endregion
	public void onAdAlreadyLoaded(string adUnitId)
	{
		Debug.Log ("onAdAlreadyLoaded "+adUnitId);
	}
	public void onAdRequestBlocked(int seconds, string adUnitId)
	{
		Debug.Log ("onAdRequestBlocked for "+seconds+" seconds, adunitid:"+adUnitId);
	}

    public void askRuntimePermissions()
    {
        zaprHelperScript.requestRuntimePermissions();
        showToast("requestRuntimePermissions");
    }



	// Shows Android Toast on device screen
	private void showToast (string message)
	{
		#if UNITY_ANDROID
		unityPlayer = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		activity = unityPlayer.GetStatic<AndroidJavaObject> ("currentActivity");
		if (activity != null) {
			Debug.Log ("Showing toast : " + message);
			AndroidJavaClass toast = new AndroidJavaClass ("android.widget.Toast");
			AndroidJavaObject toast1 = toast.CallStatic<AndroidJavaObject> ("makeText", new object[] {
				activity,
				message,
				0
			});
			toast1.Call ("show");
		}
		#else
			Debug.Log("No Toast");
		#endif
	}
}
