/*******************************************************
 * Copyright (C) 2016-2017 Red Brick Lane Marketing Pvt Ltd - All Rights Reserved
 * 
 * This file is part of Zapr Unity Plugin and is being maintained by Udayan.
 * 
 * Zapr Unity Plugin can not be copied and/or distributed without the explicit
 * permission of  Red Brick Lane Marketing Pvt Ltd
 *******************************************************/

using UnityEngine;
using System.Collections;

// Zapr Video Ad Plugin
public class ZaprVideoAdPlugin : MonoBehaviour
{
	public ZaprVideoAdPlugin ()
	{
		#if UNITY_ANDROID || DEBUG
		activityContext = new AndroidJavaClass ("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject> ("currentActivity");
		zaprVideoAdPlugin = new AndroidJavaObject ("com.redbricklane.zapr.unity.video.ZaprVideoPlugin");
		#endif
	}


	private AndroidJavaObject activityContext = null;
	private AndroidJavaObject zaprVideoAdPlugin = null;
	private bool isTestMode = false;
	private IZaprVideoAdListener listener;
	private IZaprAdStatusListener mAdStatusListener;
	private string customAdServerUrl = null;
	private bool isCloseButtonEnabled = false;
	private bool permissionRequestEnabled = false;
	private bool isPreCachingEnabled = true;
	// User Age in years
	private int userAge = -1;
	// User gender. Options: m / f
	private string userGender = null;
	// Call this method to fetch Video ad from server.
	// It is mandatory to call this method before calling showVideoAd() method.
	public void loadVideoAd (string adUnitId, IZaprVideoAdListener videoAdListener)
	{
		#if UNITY_ANDROID || DEBUG
		listener = videoAdListener;
		if (adUnitId != null) {
			string handlerName = gameObject.name;

			// Setting optional parameters before calling loadAd
			if (isTestMode) {
				zaprVideoAdPlugin.Call ("setTestModeEnabled", isTestMode);
			}
			if (!isPreCachingEnabled)
			{
				zaprVideoAdPlugin.Call("setPreCachingEnabled", isPreCachingEnabled);
			}
			if (customAdServerUrl != null) {
				zaprVideoAdPlugin.Call ("setAdServerUrl", customAdServerUrl);
			}
			if (permissionRequestEnabled) {
				zaprVideoAdPlugin.Call ("setPermissionRequestEnabled", permissionRequestEnabled);
			}
			if (userAge > 0 || userGender != null) {
				zaprVideoAdPlugin.Call ("setUserInfo", new object[2]{ userAge, userGender });
			}
			zaprVideoAdPlugin.Call ("enableVideoAdCloseButton", isCloseButtonEnabled);

			// Call for ad
			zaprVideoAdPlugin.Call ("loadVideoAd", new object[3]{ activityContext, adUnitId, handlerName });
		} else {
			Debug.Log ("AdUnit Id is invalid");
			if (videoAdListener != null) {
				videoAdListener.onVideoAdError ("AdUnitId is invalid");
			}
		}
		#endif
	}

	// Call this method to register for ad status
	public void setAdStatusListener(IZaprAdStatusListener adStatusListener)
	{
		#if UNITY_ANDROID
		mAdStatusListener = adStatusListener;
		#endif
	}

	// Call this method to start caching of Video ad, only if pre-caching is disabled.
	// Call this method only after receiving onResponseReceived() callback.
	public void cacheVideoAd()
	{
		#if UNITY_ANDROID   
		zaprVideoAdPlugin.Call ("cacheVideoAd");
		#endif
	}

	// Call this method to show Video ad to user.
	// Call this method only after receiving onVideoAdReady() callback
	public void showVideoAd ()
	{
		#if UNITY_ANDROID
		zaprVideoAdPlugin.Call ("showVideoAd");
		#endif
	}

	// Call this method before closing the application.
	// This cleans up resources used by Zapr Video Ad SDK
	public void destroy ()
	{
		#if UNITY_ANDROID
		if (zaprVideoAdPlugin != null) {
			zaprVideoAdPlugin.Call ("destroyVideoAd");
		}
		#endif
	}

	// (optional) Set the log level of Zapr SDK
	// Default log level is : error
	public void setLogLevel (string logLevel)
	{
		if (logLevel != null) {
			logLevel = logLevel.ToLower ();
			if (logLevel.Equals ("verbose")
				|| logLevel.Equals ("debug")
				|| logLevel.Equals ("info")
				|| logLevel.Equals ("warn")
				|| logLevel.Equals ("error")
				|| logLevel.Equals ("none")) {
				zaprVideoAdPlugin.Call ("setLogLevel", logLevel);
			}
		}
	}

	// (optional) Enable/Disable pre-caching of video ad. 
	// Pre-caching is enabled by default. 
	// Note: If you need to hold auto pre-caching of video ad, 
	// then disable pre-caching and call cacheVideoAd() after getting onResponseReceived() callback. 
	public void setPreCachingEnabled(bool enablePreCaching)
	{
		isPreCachingEnabled = enablePreCaching;
	}

	// (optional) Enable test mode to request test ads during integrtion.
	// Note: Test mode must be disabled in production
	public void setTestModeEnabled (bool enableTestMode)
	{
		isTestMode = enableTestMode;
	}

	// (Optional) Set custom ad server URL if required.
	public void setAdServerUrl (string adServerUrl)
	{
		customAdServerUrl = adServerUrl;
	}

	// Enable/Disable close button on video ad.
	// Default: Disabled.
	// When disabled, close button will appear either after video playing is complete
	// or after video ad skip time for skippable ads.
	// If enabled, close button will appear in video ads
	public void enableVideoAdCloseButton (bool enable)
	{
		isCloseButtonEnabled = enable;
	}

	// (Optional) Enable Rumtime permission request for Android M+ devices.
	// If enabled, runtime permissions will be requested to user when ad is first called.
	// Disabled by default.
	public void enableRuntimePermissionRequest (bool enable)
	{
		permissionRequestEnabled = enable;
	}

	// (Optional) Set user age and gender in ad request.
	// If user age and gender is passed, more targeted ads can be shown to user.
	// Note: Pass user age / gender only if correct user info is available.
	public void setUserInfo (int age, string gender)
	{
		if (age > 0 && age < 120) {
			userAge = age;
		}
		if (gender != null) {
			userGender = gender;
		}
	}

	#region Android Callbacks

	void onZaprVideoAdResponseReceived(string vastXml)
	{
		if (listener != null)
		{
			listener.onResponseReceived(vastXml);
		}
	}

	void onZaprVideoAdReady (string vastXml)
	{
		if (listener != null) {
			listener.onVideoAdReady (vastXml);
		}
	}

	void onZaprVideoAdStarted (string message)
	{
		if (listener != null) {
			listener.onVideoAdStarted ();
		}
	}

	void onZaprVideoAdClicked (string message)
	{
		if (listener != null) {
			listener.onVideoAdClicked ();
		}
	}

	void onZaprVideoAdFinished (string message)
	{
		if (listener != null) {
			listener.onVideoAdFinished ();
		}
	}

	void onZaprVideoPlayerClosed (string message)
	{
		if (listener != null) {
			listener.onVideoPlayerClosed ();
		}
	}

	void onZaprVideoAdError (string error)
	{
		if (listener != null) {
			listener.onVideoAdError (error);
		}
	}

	#endregion

	void onZaprVideoAdAlreadyLoaded(string adUnitId)
	{
		if(mAdStatusListener != null){
			mAdStatusListener.onAdAlreadyLoaded (adUnitId);
		}
	}
	void onZaprVideoAdRequestBlocked(string response)
	{			
		if(mAdStatusListener != null){
			string[] s = response.Split ('|');
			mAdStatusListener.onAdRequestBlocked (int.Parse(s[0]),s[1]);
		}	
	}
}
