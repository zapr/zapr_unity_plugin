﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZaprHelperScript : MonoBehaviour {

    private AndroidJavaObject zaprHelper = null;
    private AndroidJavaObject mContext;

	// Use this for initialization
	void Start () {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        mContext = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        zaprHelper = new AndroidJavaObject("com.redbricklane.zapr.unity.datasdk.ZaprHelperUnityPlugin");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void requestRuntimePermissions()
    {
        #if UNITY_ANDROID || DEBUG

        if (mContext != null)
        {
            // Request runtime permissions
            zaprHelper.Call("requestRuntimePermissions", mContext);
        }
        #endif
    }
}
