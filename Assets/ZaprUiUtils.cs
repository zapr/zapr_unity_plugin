﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZaprUiUtils : MonoBehaviour {
	private AndroidJavaObject activityContext = null;
	private AndroidJavaObject zaprUiUtils = null;
	public ZaprUiUtils ()
	{
		#if UNITY_ANDROID || DEBUG
		activityContext = new AndroidJavaClass ("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject> ("currentActivity");
		zaprUiUtils = new AndroidJavaObject ("com.redbricklane.zapr.unity.banner.ZaprUiUtils");
		#endif
	}

	public void updateUi()
	{
		if (activityContext != null) {
			zaprUiUtils.Call ("updateUi", activityContext);
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
